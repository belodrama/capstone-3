import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import NavBar from './components/NavBar';
import Menu from './pages/Menu'
import Orders from './pages/Orders'
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import SpecificItem from './pages/SpecificItem'
import Cart from './pages/Cart'
//import EmptyCart from './pages/EmptyCart';
import Error from './pages/Error';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { UserProvider } from './UserContext'
import { useState, useEffect } from 'react';

export default function App() {

  
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
    setUser({
      id: null,
      isAdmin: null
    })
  }

  useEffect(() => {
    fetch(`${ process.env.REACT_APP_API_URL }/users/details`, {
      headers: {
        Authorization: `Bearer ${ localStorage.getItem('token') }`
      }
    })
    .then(res => res.json())
    .then(data => {
      if(typeof data._id !== "undefined"){

        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }else{
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })

  }, [])

  return ( 
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>  
        <NavBar/>
        <Switch>
          <Route exact path="/" component={Home}/>
          <Route exact path="/menu" component={Menu}/>
          <Route exact path="/menu/:itemId" component={SpecificItem}/>
          <Route exact path="/register" component={Register}/>
          <Route exact path="/login" component={Login}/>
          <Route exact path="/orders" component={Orders}/>
          <Route exact path="/cart" component={Cart}/>
          <Route component={Error}/>
        </Switch>
      </Router>
  </UserProvider>
  );
}


