import { useState, useEffect } from 'react';
import Order from './Order';
import { Container, Row, Col } from 'react-bootstrap';



export default function OrderHistory({orderedItems}){

	const [orders, setOrders] = useState([]);

	useEffect(() => {
		
		const ordersArr = orderedItems.map(order => {

			return(
					<Order ordersProp={order} key={order._id}/>
				)		 			
		});

		setOrders(ordersArr);
			
	}, [orderedItems]);

	return(
		<Container className="mt-4">
			<Row>
				<Col sm="2">
					<h3>your order</h3>
				</Col>
				<Col>
					{orders}
				</Col>
				<Col sm="2">
				</Col>
			</Row>
		</Container>
	);
}