import { useState, useEffect } from 'react';
import { Table, Button, Modal, Form } from "react-bootstrap";
import { Container } from 'react-bootstrap';
import Swal from 'sweetalert2';


export default function AdminView(menuProps){

	const { menuItems, fetchData } = menuProps;

	
	const [itemId, setItemId] = useState('');
	const [menu, setMenu] = useState([]);
	const [showEdit, setShowEdit] = useState(false);
	const [showAdd, setShowAdd] = useState(false);
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [category, setCategory] = useState("");
	const [price, setPrice] = useState("");

	const openAdd = () => setShowAdd(true);

	const closeAdd = () => setShowAdd(false);

	const openEdit = (itemId) => {
		
		fetch(`${ process.env.REACT_APP_API_URL }/products/${ itemId }`)
		.then(res => res.json())
		.then(data => {

			setItemId(data._id)
			setName(data.name);
			setDescription(data.description);
			setCategory(data.category)
			setPrice(data.price);

		
		})
		setShowEdit(true)
	}

	const closeEdit = () => {
		setShowEdit(false);
		setName("");
		setDescription("");
		setCategory("")
		setPrice(0);
	}

	useEffect(() => {

		const menuArr = menuItems.map(item => {
			return(
				<tr key={item._id}>
					<td>{item.category}</td>
					<td>{item.name}</td>
					<td>{item.description}</td>
					<td>{item.price}</td>
					<td>
						{item.isActive
							? <span>Active</span>
							: <span>Archived</span>
						}
					</td>
					<td>
						{item.isAvailable
							? <span>Available</span>
							: <span>Out of Stock</span>
						}
					</td>
					<td>
						<Button variant="warning mx-1" size="sm" onClick={() => openEdit(item._id)}>Update</Button>
						{item.isActive
							?<Button variant="secondary mx-1" size="sm" onClick={() => archiveToggle(item._id, item.isActive)}>Archive</Button>
							:<Button variant="dark" size="sm" onClick={() => archiveToggle(item._id, item.isActive)}>Unarchive</Button>
						}
							
					</td>
				</tr>

			)
		})

		setMenu(menuArr)

	}, [menuItems])


	const addItem = (e) => {
		e.preventDefault();

		fetch(`${ process.env.REACT_APP_API_URL }/products`,{
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				category: category,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data===true){
				fetchData();
				Swal.fire({
					title: "Update Successful",
					icon: "success",
					text: "New menu item is now available."
				})
				setName("");
				setDescription("");
				setCategory("");
				setPrice(0);
				closeAdd();

			}else{
				fetchData();
				Swal.fire({
					title: "Update Error",
					icon: "error",
					text: "New menu item was not added. Try again."
				})
			}
			
		})
	}

	const editItem = (e, itemId) => {
		e.preventDefault();

		fetch(`${ process.env.REACT_APP_API_URL }/products/${ itemId }`, {
			method: 'PUT',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				category: category,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data===true){
				fetchData();
				Swal.fire({
					title: "Update Successful",
					icon: "success",
					text: "Menu item is updated."
				})
				closeEdit()

			}else{
				fetchData();
				Swal.fire({
					title: "Something went wrong.",
					icon: "error",
					text: "Please try again."
				})
			}
			
		})
	}


	const archiveToggle = (itemId, isActive) => {
		fetch(`${ process.env.REACT_APP_API_URL }/products/${ itemId }/archive`, {
			method: 'PUT',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				fetchData();
			}else{
				fetchData();
			}			
		})
	}

	return(
		<Container fluid>
			<div className="text-center my-4">
				<h2>admin dashboard</h2>
				<div className="d-flex justify-content-center">
					<Button variant="danger" onClick={openAdd}>Add New Menu Item</Button>
				</div>
			</div>
			<Table hover responsive>
				<thead className="bg-light text-dark">
					<tr>
						<th>Category</th>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th>Stocks</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{menu}
				</tbody>
			</Table>

			{/*Edit item*/}
			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => editItem(e, itemId)}>
				<Modal.Header closeButton>
					<Modal.Title>Edit Item</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					
						<Form.Group controlId="itemName">
							<Form.Label>Name</Form.Label>
							<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="itemDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)}required/>
						</Form.Group>
						<Form.Group controlId="itemCategory">
							<Form.Label>Category</Form.Label>
							<Form.Control type="text" value={category} onChange={e => setCategory(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="itemPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control type="number" value={price} onChange={e => setPrice(e.target.value)} required/>
						</Form.Group>
					
				</Modal.Body>
				<Modal.Footer>
					<Button variant="secondary" onClick={closeEdit}>Close</Button>
					<Button variant="warning" type="submit">Save</Button>
				</Modal.Footer>
				</Form>
			</Modal>			

			{/*Add new item*/}
			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={e => addItem(e)}>
				<Modal.Header closeButton>
					<Modal.Title>Add Menu Item</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Form.Group controlId="itemName">
						<Form.Label>Name</Form.Label>
						<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required/>
					</Form.Group>
					<Form.Group controlId="itemDescription">
						<Form.Label>Description</Form.Label>
						<Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)}required/>
					</Form.Group>

					<Form.Group controlId="itemCategory">
						<Form.Label>Category</Form.Label>
						<Form.Control type="text" value={category} onChange={e => setCategory(e.target.value)} required/>
					</Form.Group>

					<Form.Group controlId="itemPrice">
						<Form.Label>Price</Form.Label>
						<Form.Control type="number" value={price} onChange={e => setPrice(e.target.value)} required/>
					</Form.Group>

				</Modal.Body>
				<Modal.Footer>
					<Button variant="secondary" onClick={closeAdd}>Close</Button>
				
					<Button variant="danger" type="submit">Save</Button>
				</Modal.Footer>
				</Form>
			</Modal>	
		</Container>
	)
};