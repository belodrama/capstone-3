import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Item({menuProp}){

	const { _id, name, description, price, category } = menuProp;

	return(
		<Card className="mb-3">
		<Card.Header>{category}</Card.Header>
		<Card.Body>
			<Card.Title>{name}</Card.Title>
			<Card.Text>
				{description}
			</Card.Text>
			<Link className="btn btn-danger btn-sm" to={`/menu/${_id}`}>View Item</Link>
		</Card.Body>
	</Card>
	)
}
