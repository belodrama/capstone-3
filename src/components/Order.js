import { Card} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Order({ordersProp}){
	
	const { _id, name, purchasedItem,purchasedOn, totalAmount} = ordersProp;

	return(
		<Card className="mb-3">		
			<Card.Body>
				<Card.Title>
				<h3 className="text-danger">{purchasedItem[0].quantity} x {purchasedItem[0].name}</h3>
				</Card.Title>
				<Card.Text>
					<h5>PHP {totalAmount}</h5>
					Reference No.: {_id}
					<br/>
					{purchasedOn}
				</Card.Text>	
			</Card.Body>
		</Card>
	)

}
