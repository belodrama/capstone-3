import { useContext } from 'react'
import { Navbar, Nav } from 'react-bootstrap'
import { Link, useHistory } from 'react-router-dom'
import UserContext from "../UserContext"


export default function NavBar(){


	const { user, unsetUser } = useContext(UserContext);

	
	const history = useHistory();

	const logout = () => {
		unsetUser();
		history.push('/login');
	}

	let accountNav = (!user.id ) ? (
		<>
			<Link className="nav-link" to="/register">Register</Link>
	   		<Link className="nav-link" to="/login">Log In</Link>
		</>
	) : (
		<>
			<Link className="nav-link" to="/cart">Cart</Link>
			<Link className="nav-link" to="/orders">Orders</Link>
			<Nav.Link onClick={logout}>Log Out</Nav.Link>
		</>
	);

	return(
		<Navbar bg="dark" variant="dark" expand="lg">
			<Link className="navbar-brand" to="/"><strong>carlo's</strong> oven baked ribs</Link>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ml-auto">
	   				<Link className="nav-link" to="/menu">Menu</Link>
					{/*<Link className="nav-link" to="/cart">Cart</Link>*/}
					{/*<Link className="nav-link" to="/orders">Orders</Link>*/}	   				

	   				{accountNav}
	    		</Nav>
	    	</Navbar.Collapse>
		</Navbar>
	)
}
