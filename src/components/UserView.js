import { useState, useEffect } from 'react';
import Item from './Item';
import { Container, Row, Col } from 'react-bootstrap';



export default function UserView({menuItems}){

	const [menu, setMenu] = useState([]);

	useEffect(() => {
		
		const menuArr = menuItems.map(item => {

			if(item.isActive === true){
				return(
				<Item menuProp={item} key={item._id}/>
			);	
			}else{
				return null;
			}			
		});

		setMenu(menuArr);
			
	}, [menuItems]);

	return(
		<Container className="mt-4">
			<Row>
				<Col sm="2">
					<h3>our menu</h3>
				</Col>
				<Col sm="8">
					{menu}
				</Col>
				<Col sm="2">

				</Col>
			</Row>
		</Container>
	);
}