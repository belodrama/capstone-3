import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Col, Row } from 'react-bootstrap';
import { Link, useParams, useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';

import UserContext from '../UserContext';

export default function SpecificItem(){

    const { user } = useContext(UserContext);
    const { itemId } =  useParams();

	const [category, setCategory] = useState(" ");
	const [name, setName] = useState(" ");
	const [description, setDescription] = useState(" ");
	const [price, setPrice] = useState(" ");
    const [id, setId] = useState("");
    const [qty, setQty] = useState(1);
    const [cart, setCart]= useState(1);

	

	useEffect(() => {
		fetch(`${ process.env.REACT_APP_API_URL }/products/${itemId}`)
		.then(res => res.json())
		.then(data => {
			//console.log(data)
			
			setName(data.name);
			setCategory(data.category);
			setDescription(data.description);
			setPrice(data.price);

		});

	}, []);

	const addToCart = () => {
		let alreadyInCart = false;
		let productIndex;
		let cart = [];

		if(localStorage.getItem('cart')){
			cart = JSON.parse(localStorage.getItem('cart'))
		}

		for(let i = 0; i < cart.length; i++){
			if(cart[i].productId === id){
				alreadyInCart = true
				productIndex = i
			}
		}
		if(alreadyInCart){
			cart[productIndex].quantity += qty
			cart[productIndex].subtotal = cart[productIndex].price * cart[productIndex].quantity
		}else{
			cart.push({
				'productId': itemId,
				'name': name,
				'price': price,
				'quantity': qty,
				'subtotal': price * qty
			})


		}
		localStorage.setItem('cart', JSON.stringify(cart))
        
        alert("Added to cart")

	}

	return(

		<Container className="mt-4">
			<Link className="btn btn-outline-secondary btn-sm mb-3" to="/menu">Back to Menu</Link>
			<Card>
				<Card.Header>{category}</Card.Header>
					<Card.Body>
						<Row>
							<Col sm="10">
								<Card.Title>{name}</Card.Title>
							</Col>
							<Col sm="2">
								<h5>PHP {price}</h5>
							</Col>
						</Row>
						<Row>
							<Col>
								<Card.Text>{description}</Card.Text>
							</Col>
						</Row>
						<Row className="mt-3">
							<Col>
								{user.id !== null
									? <Button variant="danger" onClick={() => addToCart()}>Add to Cart</Button>
									: <Link className="btn btn-danger btn-block" to="/login">Log In to Continue Shopping</Link>
								}
							</Col>
						</Row>
					</Card.Body>
			</Card>
		</Container>
	)

};