import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Form, Table } from 'react-bootstrap';
import { Link, useParams, useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';
import { Redirect} from 'react-router-dom';
import EmptyCart from './EmptyCart';

export default function Cart(){
	const [total, setTotal] = useState(0)
	const [cart, setCart] = useState([])
	const [id, setId] = useState("")
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [qty, setQty] = useState(1)
	const [price, setPrice] = useState(0)
	const [cartTags, setCartTags] = useState([]);


	const qtyInput = (productId, value) => {

		let tempCart = [...cart]

		for(let i = 0; i < tempCart.length; i++){

			if(tempCart[i].productId === productId){
				tempCart[i].quantity = parseFloat(value)
				tempCart[i].subtotal = tempCart[i].price * tempCart[i].quantity
			}
		}
		setCart(tempCart);
		localStorage.setItem('cart', JSON.stringify(tempCart));
	}

	const removeItem = (productId) => {
		let tempCart = [...cart]

		tempCart.splice([tempCart.indexOf(productId)], 1)

		setCart(tempCart);
		localStorage.setItem('cart', JSON.stringify(tempCart));	
	}

	const purchasedItems = cart.map(product => {
		return {
			name: product.name,
			productId: product.productId,
			quantity: product.quantity
		}
	})

	let countProductOrder = purchasedItems.length;

	const checkOut = () => {										
											
		fetch(`${process.env.REACT_APP_API_URL}/orders/checkOut`,{															
			method : "POST",								
				headers:{								
					Authorization: `Bearer ${localStorage.getItem("token")}`,							
					"Content-Type" : "application/json"							
				},								
			body : JSON.stringify({								
				purchasedItem : purchasedItems,		
				totalQuantity : countProductOrder,
				totalAmount : total							
			})								
											
		})									
		.then(res => res.json())
		.then(data => {									
			//console.log(data)									
			if(data){								
			localStorage.removeItem('cart')
							
				setCart([]);							
				setCartTags([]);							
				setTotal(0);							
			}								
			else{								
				alert("Error.")				
			}								
		})									
	}

	useEffect(()=> {										
		if(localStorage.getItem('cart')){									
			setCart(JSON.parse(localStorage.getItem('cart')))								
		}									
	}, []);

	useEffect(()=> {										
											
		const cartsWithTags = cart.map(product => {									
											
			return(								
											
				<tr key={product.productId}>							
					<td>{product.name}</td>						
					<td>PHP {product.price}</td>						
					<td>						
											
						<div class="d-none d-md-flex input-group">
							<div class="input-group-prepend">				
								<Button className="btn btn-secondary" onClick={() => qtyInput(product.productId, product.quantity - 1)}>-</Button>			
							</div>				
								<Form.Control className="text-center" type="number" min="1" value={product.quantity} onChange={e => qtyInput(product.productId, e.target.value)} required/>			
											
							<div class="input-group-append">				
								<Button className="btn btn-secondary" onClick={() => qtyInput(product.productId, product.quantity + 1)}>+</Button>			
							</div>				
						</div>					
											
											
					</td>						
											
					<td>						
						PHP {product.subtotal}					
					</td>						
					<td>						
						<Button className="btn btn-danger" onClick={() => removeItem(product.productId)}>Remove</Button>					
					</td>						
				</tr>							
											
			)								
											
		})

		let tempTotal = 0

		cart.forEach((item)=> {									
			tempTotal += item.subtotal								
		})									
																				
		setTotal(tempTotal)									
											
		setCartTags(cartsWithTags);									
											
	}, [cart])										
											
	const cartName = localStorage.getItem('cart');										
											
				
	if (!cartName ){
		return(
			<EmptyCart/>	
		)	
	}

	return(
		<Container >	
			<h2 class="text-center my-4">your cart</h2>									
			<Table  hover responsive>								
					<thead className="bg-light ">						
						<tr>					
							<th>Product Name</th>				
							<th>Price</th>				
							<th className="text-center">Quantity</th>				
							<th>Subtotal</th>				
							<th></th>				
						</tr>					
					</thead>						
					<tbody>						
											
						{cartTags}					
											
						<tr>					
							<td colspan="3">				
								<Button className="btn btn-block btn-warning" onClick={() => checkOut()}>Checkout</Button>			
							</td>				
											
							<td colspan="2">				
								Total: {total}			
							</td>				
						</tr>					
											
					</tbody>						
											
			</Table>								
											
											
		</Container>
	)
}