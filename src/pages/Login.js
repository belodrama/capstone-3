import { useState, useEffect, useContext } from "react";
import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import UserContext from '../UserContext'
import Swal from 'sweetalert2';


export default function Login(){
    
    const [userName, setUserName] = useState('');
    const [password, setPassword] = useState('');

    const { user, setUser } = useContext(UserContext);

    const [loginButton, setLoginButton] = useState(false);

    const loginUser = (e) => {
        e.preventDefault();

        fetch(`${ process.env.REACT_APP_API_URL }/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                userName: userName,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            
            if(typeof data.access !== "undefined"){

                localStorage.setItem('token', data.access);
                retrieveUserDetails(data.access)
                
                }else{
                    Swal.fire({
                        title: "Login error",
                        icon: "error",
                        text: "Please check your login details. Try again."
                    });

                } 
        })

    };

    useEffect(()=>{
        if(userName !== '' && password !== ''){
            setLoginButton(true)
        }else{
            setLoginButton(false)
        }
    }, [userName, password])

    const retrieveUserDetails = (token) => {
        fetch(`${ process.env.REACT_APP_API_URL }/users/details`, {
            headers: {
            Authorization: `Bearer ${token}`
        }
    })

    .then(res => res.json())
    .then(data => {

        
        setUser({
            id: data._id,
            isAdmin: data.isAdmin
            })
        })
    }

    if(user.id !== null){
        return <Redirect to="/" />
    }

    return(
        <Container>
            <Form className="mt-3" onSubmit={e => loginUser(e)}>
                <Form.Group as={Row}>
                    <Form.Label column sm={2}>Username</Form.Label>
                    <Col sm={10}>
                    <Form.Control type="text" placeholder="Enter username" value={userName} onChange={e => setUserName(e.target.value)} required />
                    </Col>
                </Form.Group>


                <Form.Group as={Row}>
                    <Form.Label column sm={2}>Password</Form.Label>
                    <Col sm={10}>
                    <Form.Control type="password" placeholder="Enter password" value={password} onChange={e => setPassword(e.target.value)} required />
                    </Col>
                </Form.Group>


                {loginButton ?

                <Button variant="danger" type="submit">Log In</Button>
                :

                <Button variant="danger" type="submit" disabled>Log In</Button>
                }
            </Form>
        </Container>    
    )
}