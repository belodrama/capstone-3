import Banner from '../components/Banner';
import { Link } from 'react-router-dom';
import { Container } from 'react-bootstrap'

export default function EmptyCart(){
	
	const data = {
		title: "Nothing to see here",
		content: "Your shopping cart is empty.",
		destination: "/menu",
		label: "Start order"
	}

	return(
		<>	
			<Link className="btn btn-warning btn-block btn-lg" to="/orders">View all Orders</Link>
			<Banner bannerProps = {data}/>
			
		</>
	);
}