import { useState, useEffect, useContext } from "react";
import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import { Redirect, useHistory } from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2';


export default function Register() {

	const [userName, setUserName] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [verifyPassword, setVerifyPassword] = useState('');
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	//const [gender, setGender] = useState('');

	const { user } = useContext(UserContext);
	
	const history = useHistory();

	const [registerButton, setRegisterButton] = useState(false);

	const registerUser = (e) => {
		e.preventDefault();
		
		fetch(`${ process.env.REACT_APP_API_URL }/users/checkUser`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				userName: userName
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				Swal.fire({
					title: 'Registration error',
					icon: 'error',
					text: 'Username is already taken. Please choose another.'
				})
			}else{
				fetch(`${ process.env.REACT_APP_API_URL }/users/register`, {
					method: "POST",
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						userName: userName,
						password: password,
						firstName: firstName,
						lastName: lastName,
						email: email,
						/*gender: gender*/
						
					})
				})
				.then(res => res.json())
				.then(data => {
					if(data === true){
						Swal.fire({
							title: 'Registration Successful',
							icon: 'success',
							text: 'Your account has been created.'
						})

						history.push('/login')
						
					}else{
						Swal.fire({
							title: 'Registration error',
							icon: 'error',
							text: 'Please try again.'
						})

					}
				})
			}
		})
		
	}

	useEffect(() => {

		if((userName !== '' && password !== '' && verifyPassword !== '') && (password === verifyPassword)){
			setRegisterButton(true)

		}else{
			setRegisterButton(false)
		}

	}, [userName, password, verifyPassword])

	if(user.id != null){
		return <Redirect to="/" />
	}

	return(
		<Container>
			<Form className="mt-3" onSubmit={(e)=> registerUser(e)}>
				<Form.Group as={Row}>
					<Form.Label column sm={2}>Username</Form.Label>
					<Col sm={10}>
					<Form.Control type="text" placeholder="Enter username" value ={userName} onChange={e => setUserName(e.target.value)} required/>
					</Col>
				</Form.Group>
 
 				<Form.Group as={Row}>
					<Form.Label column sm={2}>Email Address</Form.Label>
					<Col sm={10}>
						<Form.Control type="email" placeholder="Enter email address" value={email} onChange={e => setEmail(e.target.value)} required/>
					</Col>
				</Form.Group>

				<Form.Group as={Row}>
					<Form.Label column sm={2}>Password</Form.Label>
					<Col sm={10}>
						<Form.Control type="password" placeholder="Enter password" value={password} onChange={e => setPassword(e.target.value)} required/>
					</Col>
				</Form.Group>

				<Form.Group as={Row}>
					<Form.Label column sm={2}>Verify Password</Form.Label>
					<Col sm={10}>
						<Form.Control type="password" placeholder="Enter password" value={verifyPassword} onChange={e => setVerifyPassword(e.target.value)} required/>
					</Col>
				</Form.Group>


				<Form.Group as={Row}>
					<Form.Label column sm={2}>First Name</Form.Label>
					<Col sm={10}>
						<Form.Control type="text" placeholder="Enter first name" value={firstName} onChange={e => setFirstName(e.target.value)} required/>
					</Col>
				</Form.Group>

				<Form.Group as={Row}>
					<Form.Label column sm={2}>Last Name</Form.Label>
					<Col sm={10}>
						<Form.Control type="text" placeholder="Enter last name" value={lastName} onChange={e => setLastName(e.target.value)} required/>
					</Col>
				</Form.Group>


				{registerButton ?

				<Button variant="danger" type="submit">Submit</Button>
				:

				<Button variant="danger" type="submit" disabled>Submit</Button>
				}

			</Form>
		</Container>
	)
}