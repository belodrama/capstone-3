//import Banner from '../components/Banner';
import { Carousel } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Home(){

    return(
        <Carousel>
            <Carousel.Item>
            <img
            className="d-block w-100"
            src="./images/carloshome1.png"
            alt="Carlo's group meals"
            />
            <Carousel.Caption>
                <Link className="btn btn-warning btn-lg" to="/menu">View Menu</Link>
            </Carousel.Caption>
        </Carousel.Item>
        
        <Carousel.Item>
            <img
            className="d-block w-100"
            src="./images/carloshome2.png"
            alt="Ribs, Ribs, Baby"
            />
        </Carousel.Item>
    </Carousel>
    
    )
}

    
