import { useState, useEffect, useContext } from 'react';
import UserView from '../components/UserView';
import AdminView from '../components/AdminView';
import UserContext from '../UserContext';


export default function Menu(){

	const { user } = useContext(UserContext);

	const [menu, setMenu] = useState([]);

	const fetchData = () => {
		fetch(`${ process.env.REACT_APP_API_URL }/products/all`)
		.then(res => res.json())
		.then(data => {
			setMenu(data)			
		});
	}

	useEffect(() => {
		fetchData()
	}, [])

	return(
		<>

		{(user.isAdmin === true)
			? <AdminView menuItems ={menu} fetchData={fetchData}/>
			: <UserView menuItems ={menu} fetchData={fetchData}/>
		}
		</>
	)
}