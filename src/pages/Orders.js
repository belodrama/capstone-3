import { useState, useEffect, useContext } from 'react';
import OrderHistory from '../components/OrderHistory';
import UserContext from '../UserContext';


export default function Orders(){

	const { user } = useContext(UserContext);

	const [orders, setOrders] = useState([]);

	const fetchData = () => {
		fetch(`${ process.env.REACT_APP_API_URL }/orders/myOrder`,{headers : {		
				Authorization : `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setOrders(data)			
		});
	}

	useEffect(() => {
		fetchData()
	}, [])

	return(
		<>

			<OrderHistory orderedItems ={orders} fetchData={fetchData}/>
		
		</>
	)
}